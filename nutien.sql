﻿DROP DATABASE IF EXISTS tienda;
CREATE DATABASE IF NOT EXISTS tienda;
USE tienda;
CREATE TABLE productos (
idp int,
  peso int,
  nombrep varchar(40),
  PRIMARY KEY(idp)
);
CREATE TABLE clientes(
idc int,
  nombrec varchar(40),
  PRIMARY KEY(idc)
);
CREATE TABLE compran(
fecha date,
  cantidad int,
  idp int,
  idc int,
  PRIMARY KEY(idp, idc),
  CONSTRAINT comproductos FOREIGN KEY(idp) REFERENCES productos (idp),
  CONSTRAINT comclientes FOREIGN KEY (idc) REFERENCES clientes (idc)
);